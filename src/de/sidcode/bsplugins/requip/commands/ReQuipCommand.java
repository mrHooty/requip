package de.sidcode.bsplugins.requip.commands;

import de.sidcode.bsplugins.requip.ReQuip;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Created by felixpeinemann on 30.06.15.
 */
public class ReQuipCommand implements CommandExecutor {

    private ReQuip plugin;

    public ReQuipCommand(ReQuip plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(label.equalsIgnoreCase("requip")) {

            /*
             * Command: /requip *
             * One argument are given.
             */
            if (args.length == 1) {
                String subcmd = args[0];

                /*
                 * Command: /requip reload.
                 * Reloads the configuration.
                 */
                if(subcmd.equalsIgnoreCase("reload")) {
                    if(sender.hasPermission("requip.command.reload")) {

                        plugin.reloadConfig();

                        sender.sendMessage(plugin.prefix + "Configuration reloaded successfully!");
                        return true;
                    } else {
                        sender.sendMessage(plugin.prefix + "You don't have enough permissions to use this command!");
                        return true;
                    }
                }

                /*
                 * Command: /requip save
                 * Saves the players inventory as a kit.
                 */
                if(subcmd.equalsIgnoreCase("save")) {
                    sender.sendMessage(plugin.prefix + "You have to specify what inventory should be saved.");
                    sender.sendMessage(plugin.prefix + "  /requip save inventory");
                    sender.sendMessage(plugin.prefix + "  /requip save armor");

                    return true;
                }

                /*
                 * Command: /requip view
                 * View current kit inventory.
                 */
                if(subcmd.equalsIgnoreCase("view")) {
                    sender.sendMessage(plugin.prefix + "You have to specify what inventory you want to view.");
                    sender.sendMessage(plugin.prefix + "  /requip view inventory");
                    sender.sendMessage(plugin.prefix + "  /requip view armor");

                    return true;
                }

                /*
                 * Command: /requip getkit
                 * Clears your inventory and gives you the current kit.
                 */
                if(subcmd.equalsIgnoreCase("getkit")) {
                    if (!(sender instanceof Player)) {
                        sender.sendMessage(plugin.prefix + "This command can only be executed by a player.");
                        return true;
                    }

                    if (sender.hasPermission("requip.command.getkit")) {
                        Player player = (Player) sender;

                        player.getInventory().clear();

                        player.getInventory().setContents((ItemStack[]) plugin.loadInventory("inventory"));
                        player.getInventory().setArmorContents((ItemStack[]) plugin.loadInventory("armor"));

                        sender.sendMessage(plugin.prefix + "Now you have the current respawn kit in your inventory!");

                        return true;
                    } else {
                        sender.sendMessage(plugin.prefix + "You don't have enough permissions to use this command!");
                        return true;
                    }
                } else {
                    sender.sendMessage(plugin.prefix + "That command doesn't exists!");
                    return true;
                }

            /*
             * Command: /requip * *
             * Two arguments are given.
             */
            } else if(args.length == 2) {

                /*
                 * Command: /requip save
                 * Saves the players inventory as a kit.
                 */
                if(args[0].equalsIgnoreCase("save")) {
                    if(!(sender instanceof Player)) {
                        sender.sendMessage(plugin.prefix + "This command can only be executed by a player.");
                        return true;
                    }

                    if(sender.hasPermission("requip.command.save")) {
                        Player player = (Player) sender;

                        if(args[1].equalsIgnoreCase("inventory")) {
                            plugin.saveInventory(player.getInventory().getContents(), "inventory");

                        } else if(args[1].equalsIgnoreCase("armor")) {
                            plugin.saveInventory(player.getInventory().getArmorContents(), "armor");

                        } else {
                            player.sendMessage("Theres no inventory of this type.");
                            return true;
                        }
                        plugin.saveConfig();
                        plugin.reloadConfig();
                        player.sendMessage(plugin.prefix + "Your inventory was saved as the new respawn kit!");

                        return true;
                    } else {
                        sender.sendMessage(plugin.prefix + "You don't have enough permissions to use this command!");
                        return true;
                    }
                }

                /*
                 * Command: /requip view
                 * View current kit inventory.
                 */
                if(args[0].equalsIgnoreCase("view")) {
                    if(!(sender instanceof Player)) {
                        sender.sendMessage(plugin.prefix + "This command can only be executed by a player.");
                        return true;
                    }

                    if(sender.hasPermission("requip.command.view")) {
                        Player player = (Player) sender;

                        Inventory inv = null;
                        if(args[1].equalsIgnoreCase("inventory")) {
                            inv = Bukkit.createInventory(player, 36, "Current respawn kit: Inventory");
                            inv.setContents((ItemStack[]) plugin.loadInventory("inventory"));

                        } else if(args[1].equalsIgnoreCase("armor")) {
                            ItemStack[] tmp = (ItemStack[]) plugin.loadInventory("armor");

                            inv = Bukkit.createInventory(player, 9, "Current respawn kit: Armor");
                            inv.setItem(0, new ItemStack(Material.STAINED_GLASS_PANE, 1));
                            inv.setItem(1, new ItemStack(Material.STAINED_GLASS_PANE, 1));
                            inv.setItem(2, tmp[3]);
                            inv.setItem(3, tmp[2]);
                            inv.setItem(4, new ItemStack(Material.STAINED_GLASS_PANE, 1));
                            inv.setItem(5, tmp[1]);
                            inv.setItem(6, tmp[0]);
                            inv.setItem(7, new ItemStack(Material.STAINED_GLASS_PANE, 1));
                            inv.setItem(8, new ItemStack(Material.STAINED_GLASS_PANE, 1));

                        } else {
                            player.sendMessage("Theres no inventory of this type.");
                            return true;
                        }
                        player.openInventory(inv);

                        return true;
                    } else {
                        sender.sendMessage(plugin.prefix + "You don't have enough permissions to use this command!");
                        return true;
                    }
                }

            /*
             * Command: /requip
             * No subcommand was given.
             */
            } else {
                sender.sendMessage("ReQuip v" + plugin.getDescription().getVersion() + " made by " + plugin.getDescription().getAuthors());
                sender.sendMessage(" Website: " + plugin.getDescription().getWebsite());
                return true;
            }
        }
        return false;
    }
}
