package de.sidcode.bsplugins.requip;

import de.sidcode.bsplugins.requip.commands.ReQuipCommand;
import de.sidcode.bsplugins.requip.events.onPlayerRespawnEvent;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class ReQuip extends JavaPlugin {

    public String prefix = ChatColor.GRAY + ":: " + ChatColor.WHITE;

    @Override
    public void onEnable() {
        try {
            Metrics metrics = new Metrics(this);
            metrics.start();
            getServer().getLogger().log(Level.INFO, "Started Metrics for mcstats.org");
        } catch (IOException ex) {
            getServer().getLogger().log(Level.SEVERE, "Error while starting Metrics for mcstats.org");
        }

        loadConfiguration();
        getServer().getPluginManager().registerEvents(new onPlayerRespawnEvent(this), this);

        this.getCommand("requip").setExecutor(new ReQuipCommand(this));
    }

    private void loadConfiguration() {
        this.getConfig().addDefault("general.enabled", true);
        this.getConfig().addDefault("kit.inventory", new ArrayList<String>());
        this.getConfig().addDefault("kit.armor", new ArrayList<String>());

        this.getConfig().options().header("ReQuip (Old name: KitAfterDeath) v2 - Configuration file");
        this.getConfig().options().copyHeader(true);
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }

    public boolean saveInventory(ItemStack[] contents, String path) {
        this.getConfig().set("kit." + path, contents);
        return true;
    }

    public Object loadInventory(String path) {
        Object content = this.getConfig().get("kit." + path);
        if(content == null) return false;

        ItemStack[] inventory = null;
        if(content instanceof ItemStack[]) {
            inventory = (ItemStack[]) content;
        } else if(content instanceof List) {
            List invList = (List) content;
            inventory = (ItemStack[]) invList.toArray(new ItemStack[0]);
        }

        return inventory;
    }
}
