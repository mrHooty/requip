package de.sidcode.bsplugins.requip.events;

import de.sidcode.bsplugins.requip.ReQuip;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Created by felixpeinemann on 30.06.15.
 */
public class onPlayerRespawnEvent implements Listener {

    private ReQuip plugin;

    public onPlayerRespawnEvent(ReQuip plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    private void onPlayerRespawn(PlayerRespawnEvent e) {
        if(!plugin.getConfig().getBoolean("general.enabled")) return;

        if(!e.getPlayer().hasPermission("requip.getkit.ondeath")) return;

        e.getPlayer().getInventory().setContents((ItemStack[]) plugin.loadInventory("inventory"));
        e.getPlayer().getInventory().setArmorContents((ItemStack[]) plugin.loadInventory("armor"));

        return;
    }
}
